 import java.lang.Math;
public class Arithmetic{

                       
  
  public static void main(String args []){
    
    int numPants = 3; //stating the number of pants purchased 
    double pantCost = 34.98; //stating the cost of each pair of pants
    int numBelts = 1; //stating the number of belts purchased 
    double beltCost = 33.99; //stating thse cost of each belt 
    int numShirts = 2; //stating the number of shirts purchased
    double shirtCost = 24.99; //stating the price of each shirt
    double salesTax = 0.06; //stating the sales tax
    
    double totalCostPants = (numPants * pantCost); //finding the total cost of pants
    double totalCostBelts = (numBelts * beltCost); //finding the total cost of the belts
    double totalCostShirts = (numShirts * shirtCost); // finding the total cost of the shirts 
    
    double salesTaxShirts = (salesTax * totalCostShirts); //sales tax only shirts
    double salesTaxBelts = (salesTax * totalCostBelts); //sales tax on belts only
    double salesTaxPants = (salesTax * totalCostPants); //sales tax on pants only
    
   totalCostPants = Math.round(totalCostPants * 100.0) / 100.0; // ensures only two decimals
   totalCostBelts = Math.round(totalCostBelts * 100.0) / 100.0; // ensures only two decimals
   totalCostShirts = Math.round(totalCostShirts * 100.0) / 100.0; // ensures only two decimals
   salesTaxPants = Math.round(salesTaxPants * 100.0) / 100.0; // ensures only two decimals
   salesTaxBelts = Math.round(salesTaxBelts * 100.0) / 100.0;//ensures only two decimals
   salesTaxShirts = Math.round(salesTaxShirts * 100.0) / 100.0;//ensures only two decimals
   
    System.out.println("The total cost of the shirts was $" + totalCostShirts + " and the sales tax on them was $" + salesTaxShirts); //displays total cost and sales tax
    System.out.println("The total cost of the shirts was $" + totalCostPants + " and the sales tax on them was $" + salesTaxPants); //displays total cost and sales tax
    System.out.println("The total cost of the shirts was $" + totalCostBelts + " and the sales tax on them was $" + salesTaxBelts); //displays total cost and sales tax
   
    
 
    
    double totalPurchasePreTax = (totalCostShirts + totalCostPants + totalCostBelts); //total cost before tax
    double totalPurchasePostTax = (totalPurchasePreTax * (1 + salesTax)); //total cost after sales tax
    double totalSalesTax = (totalPurchasePostTax - totalPurchasePreTax); //total sales tax equation 
    
    totalSalesTax = Math.round(totalSalesTax * 100.0) / 100.0; //ensuring two decimals
    totalPurchasePostTax = Math.round(totalPurchasePostTax * 100.0) / 100.0;//ensures two decimals
    
 
  System.out.println("The total cost of the purchase before taxes was $" + totalPurchasePreTax); //displaying total purchase cost before tax
  System.out.println("The total sales tax on the purchase was $" + totalSalesTax); //displaying total sales tax
  System.out.println("The total cost of the purchase after taxes was $" + totalPurchasePostTax); //displaying the total cost after tax
  
  
  
  
  
  }
 
}