// Code is written by Noah Haversat for Homework # 3
// for Professor Carr's Class.  The date is September 18th, 2018 

// must import the scanner in order to allow for user inputs 

import java.util.Scanner;  //this imports the scanner

public class Convert{ 
  public static void main(String[] args){  //creating the string for the program 
    
  Scanner myScanner = new Scanner(System.in); // declaring scanner and calling constructor
  
  System.out.print("Enter how many acres of land were affected ");  //prompting the user to input the how many acres were hit by the hurricane
  double acreAmount = myScanner.nextDouble(); // creates variable for the amount of acres effected by the hurricane
    
  System.out.print("Enter how many inches of rain was recorded ");  //prompts user to input how many inches of rain were reported
  double averageRainAmount = myScanner.nextDouble(); //creates variable for the amount of rain that fell during the hurricane
  
  double rainAmountCubicMiles;  //declaring our final variable of how many cubic miles of rain fell
  
  double totalRainFall = averageRainAmount * acreAmount;  //this gives us the total amount of rain that fell over the whole area
  
  
  rainAmountCubicMiles = averageRainAmount * 27154.00 * acreAmount * 0.000000000000908169;
    
  System.out.print("a total of " + rainAmountCubicMiles + " cubic miles of rain fell during the hurricane"); //printing how much rain fell in cubic miles over the entire effected area 
    
  
  }
}