//this is the second portion of homework number 3
//the goal of this program is to prompt the user for dimesnsions of a pyramid and then
//have the program calculate and print the final volume 
//Noah Havesat CSE 002 with professor Carr
//The date is September 18th, 2018 

import java.util.Scanner;  //this imports the scanner

public class Pyramid{
   public static void main(String[] args){ //creates string for the program
     
     Scanner myScanner = new Scanner(System.in); // declaring scanner and calling constructor
     System.out.print ("Please enter the height of the pyramid "); //prompting user to ente the height 
     double pyramidHeight = myScanner.nextDouble(); //creating variable for the height of the pyramidH
     
     System.out.print ("Please enter the length of the pyramid "); //prompts user to enter length of the side of the pyramidHeight
     double pyramidLength = myScanner.nextDouble(); //creating variable for the length of the base of the pyramid
     
     System.out.print ("Please enter the width of the pyramid ");  //prompts user to enter the width of the pyramid 
     double pyramidWidth = myScanner.nextDouble(); //creating variable for the width of the pyramid
     
     double pyramidVolume = (pyramidWidth * pyramidLength * pyramidHeight) / 3;  //formula for the volume of a pyramid
    
     System.out.print("The total volume of the pyramid is " + pyramidVolume);  //prints to user the total volume of the pyramid
   
   }

} 
  