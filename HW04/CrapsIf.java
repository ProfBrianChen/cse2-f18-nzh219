//September 23, 2018
//Noah Haversat 
//CSE 002
//this program is aimed to allow the user to play craps either by entering their own faces 
//of the die or by allowing the program to randomly assign them


//we import the random number generator 
import java.util.Random;
//we import the scanner so we can ask for faces of the die if not randomly selected
import java.util.Scanner;

public class CrapsIf{
   public static void main(String[] args){
     //setting up the scanner
     Scanner myScanner = new Scanner(System.in);
     //setting up the random number generator to account for the six sided die  
     Random rand = new Random();
     int diceOne = rand.nextInt(6) + 1; //creats value for the first die
     int diceTwo = rand.nextInt(6) + 1; //creates value for the second die
     int sumDice = diceOne + diceTwo;
     String yes = "yes"; //gives user the option to prompt yes to the computer
     String no = "no"; //gives user the option to prompt yes to the computer
     
     System.out.println("Would you like to choose your numbers or have them chosen randomly?"); //asking the user if they want to pick their own number or have them chosen randomly
     System.out.println("Type 'yes' for your own numbers or 'no' for randomly chosen numbers"); //asking the user how they want to choose their numbers
     
     String decision = myScanner.nextLine(); //assigning the user input 
     
     if (decision.equals("yes")){
       System.out.println("What would you like your first number to be? (in the range of 1 - 6)"); //prompting user to input their choice of first number
          int decision1 = myScanner.nextInt();//creating variable for the number picked by the user
       System.out.println("What would you like your second number to be?(in the range of 1 - 6)"); //prompting user to input their choice of second number
          int decision2 = myScanner.nextInt(); //creating variable for the number picked by the user
          
          int sumOfDice = decision1 + decision2; //creating variable for the sum of the two dice2
         // int score; //declaring a variable for the result of the users input
         
       //this is for the specific cases of doubles
          if (decision1 == decision2){
              if (decision1 == 1) {
                System.out.println("You have rolled snake eyes");} //prints result
              else if (decision1 == 6){
                System.out.println("You have rolled Boxcars");} //prints result 
              else {
                   System.out.println("You have rolled a hard " + sumOfDice);} //printing the results
              }
       //this is for the rest of the possible inputs 
          else if (sumOfDice == 3){
            System.out.println("You have rolled an Ace Duece"); 
          }
          else if (sumOfDice == 4){
            System.out.println("You have rolled an easy four");
          }
           else if (sumOfDice == 5){
            System.out.println("You have rolled afever five");
          }
           else if (sumOfDice == 6){
            System.out.println("You have rolled an easy six");
          }
           else if (sumOfDice == 7){
            System.out.println("You have rolled a seven out");
          }
           else if (sumOfDice == 8){
            System.out.println("You have rolled an easy eight");
          }
           else if (sumOfDice == 9){
            System.out.println("You have rolled a nine");
          }
           else if (sumOfDice == 10){
            System.out.println("You have rolled an easy ten");
          }
           else {
            System.out.println("You have rolled a yo-leven");
          }
        } //end of program if user inputs yes
     
     
     //this is the beginnig of the programs path if the user inputs no
     else if (no.equals("no")) {
       
       
       System.out.println("your first number is " + diceOne); //displaying the users first number
       System.out.println("your second number is " + diceTwo); //displaying the users second number
            
     int sumOfDice2 = diceOne + diceTwo; //creating variable for the sum of the two dice2
         
       //this is for the specific cases of doubles
          if (diceOne == diceTwo){
              if (diceOne == 1) {
                System.out.println("You have rolled snake eyes");} //prints result
              else if (diceOne == 6){
                System.out.println("You have rolled Boxcars");} //prints result 
              else {
                   System.out.println("You have rolled a hard " + sumOfDice2);} //printing the results
              }
       //this is for the rest of the possible inputs 
          else if (sumOfDice2 == 3){
            System.out.println("You have rolled an Ace Duece"); 
          }
          else if (sumOfDice2 == 4){
            System.out.println("You have rolled an easy four");
          }
           else if (sumOfDice2 == 5){
            System.out.println("You have rolled afever five");
          }
           else if (sumOfDice2 == 6){
            System.out.println("You have rolled an easy six");
          }
           else if (sumOfDice2 == 7){
            System.out.println("You have rolled a seven out");
          }
           else if (sumOfDice2 == 8){
            System.out.println("You have rolled an easy eight");
          }
           else if (sumOfDice2 == 9){
            System.out.println("You have rolled a nine");
          }
           else if (sumOfDice2 == 10){
            System.out.println("You have rolled an easy ten");
          }
           else {
            System.out.println("You have rolled a yo-leven");
          }
        } //end of program if user inputs yes
     else {System.out.println("Invalid Inputs");}
            
            

    } //finalclosingbracket
} //final closing bracket
          
              
       
      
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
    
   