//we import the random number generator 
import java.util.Random;
//we import the scanner so we can ask for faces of the die if not randomly selected
import java.util.Scanner;

public class CrapsSwitch{
   public static void main(String[] args){
     //setting up the scanner
     Scanner myScanner = new Scanner(System.in);
     //setting up the random number generator to account for the six sided die  
     Random rand = new Random();
     int diceOne = rand.nextInt(6) + 1; //creats value for the first die
     int diceTwo = rand.nextInt(6) + 1; //creates value for the second die
     int sumDice = diceOne + diceTwo;
     String yes = "yes"; //gives user the option to prompt yes to the computer
     String no = "no"; //gives user the option to prompt yes to the computer
     
     System.out.println("Would you like to choose your numbers or have them chosen randomly?"); //asking the user if they want to pick their own number or have them chosen randomly
     System.out.println("Type 'yes' for your own numbers or 'no' for randomly chosen numbers"); //asking the user how they want to choose their numbers
     
     String decision = myScanner.nextLine(); //assigning the user input 
     
     //this is the path that the program will take if the user inputs yes
    
     switch (decision){
       case "yes": System.out.println("Please input your first desired value");
         int dice1 = myScanner.nextInt();
         System.out.println("Please input your second desired value");
         int dice2 = myScanner.nextInt();
         int sumOfDice = dice1 + dice2;
         boolean Doubles;
         Doubles = (dice1 == dice2);
         String equalDice = String.valueOf(Doubles);
         
         switch (equalDice){
           case "true":
             switch (sumOfDice){
               case 2: System.out.println("You have rolled snake eyes");
                 break;
               case 12: System.out.println("You have rolled box cars");
                 break;
               default: System.out.println("You have rolled a hard " + sumOfDice);
                 break;
             }
                 break;
           
           case "false": 
             switch (sumOfDice){
               case 3: System.out.println("You have rolled an ace duece");
                 break;
               case 4: System.out.println("You have rolled an easy four");
                 break;
               case 5: System.out.println("You have rolled a fever five");
                 break;
               case 6: System.out.println("You have rolled an easy six");
                 break;
               case 7: System.out.println("You have rolled a seven out");
                 break;
               case 8: System.out.println("You have rolled an easy eight");
                 break;
               case 9: System.out.println("You have rolled a nine");
                 break;
               case 10: System.out.println("You have rolled an easy ten");
                 break;
               case 11: System.out.println("You have rolled a yo-leven");
                 break;
             }
         }
         break;
           case "no":
             System.out.println("your first roll was a " + diceOne);
             System.out.println("your second roll was a " + diceTwo);
             boolean doubleNumber = (diceOne == diceTwo);
             String doubleDouble = String.valueOf(doubleNumber);
             
             switch(doubleDouble){
               case "true":  switch(sumDice){
                 case 2: System.out.println("you have rolled snake eyes");
                   break;
                 case 12: System.out.println("you have rolled box cars");
                   break;
                 default: System.out.println("You have rolled a hard " + sumDice);
                   break;
               }
                 break;
               case "false": switch(sumDice){
                 case 3: System.out.println("You have rolled an ace duece");
                 break;
               case 4: System.out.println("You have rolled an easy four");
                 break;
               case 5: System.out.println("You have rolled a fever five");
                 break;
               case 6: System.out.println("You have rolled an easy six");
                 break;
               case 7: System.out.println("You have rolled a seven out");
                 break;
               case 8: System.out.println("You have rolled an easy eight");
                 break;
               case 9: System.out.println("You have rolled a nine");
                 break;
               case 10: System.out.println("You have rolled an easy ten");
                 break;
               case 11: System.out.println("You have rolled a yo-leven");
                 break;
             }
                                        
               }
         break;
       default: System.out.println("invalid input");
         break;
             }
     
         }
         
       
     }
   
