//Noah Haversat
//Homework Five
//October 11 2018 
//This code will ask the user how many hands of five cars that user wants the computer to deal and then will calculate
//the probability of having a four of a kind, three of a kind, two pair, and a one pair

//first we need to import a scanner and random function 

import java.util.Scanner;
import java.util.Random;

//next we create our main method and class
public class HW05 {
public static void main(String[] args){
  
//asks user how many hands they want dealt 
System.out.println("How many hands would you like to deal?"); 

//then we must create the scanner
Scanner deal = new Scanner(System.in);
  
//here we declare our variables that we will need for the loops we will use
int cardOne;
int cardTwo;
int cardThree;
int cardFour;
int cardFive;
int fourKind = 0;
int threeKind = 0;
int twoPair = 0;
int onePair = 0;
int hands = deal.nextInt();
int totalHands = hands;

//we must also declare a randomizer for the cards
Random card = new Random();


//we will now enter our while loop 
while (totalHands > 0 ) {
  
//here we define the values of each card as the program runs through this loop 
cardOne = card.nextInt(52) + 1;
cardTwo = card.nextInt(52) + 1;
cardThree = card.nextInt(52) + 1;
cardFour = card.nextInt(52) + 1;
cardFive = card.nextInt(52) + 1;

//we now create our if statements for each desired value (four of a kind, three of a kind, two pair, and pair)

//first, since suit does matter in any our desired valued we will convert the cards from 1-52 to 1-13.  This wil make calcuatoions easier further in the program 
cardOne = cardOne%13;
cardTwo = cardTwo%13;
cardThree = cardThree%13;
cardFour = cardFour%13;
cardFive = cardFive%13;

//this is our if statement for the four of a kind 
if ((cardOne == cardTwo && cardTwo == cardThree && cardThree == cardFour && cardFour != cardFive) || 
    (cardOne == cardTwo && cardTwo == cardThree && cardThree == cardFive && cardFive != cardFour) ||
    (cardOne == cardTwo && cardTwo == cardFour && cardFour == cardFive && cardFive != cardThree) ||
    (cardOne == cardThree && cardThree == cardFour && cardFour == cardFive && cardFive != cardTwo) ||
    (cardTwo == cardThree && cardThree == cardFour && cardFour == cardFive && cardFive != cardOne)){
  fourKind++;
}

//now we have our lif statement for the three of a kind 
else if ((cardOne == cardTwo && cardTwo == cardThree && cardThree != cardFour && cardThree != cardFive) ||
(cardTwo == cardThree && cardThree == cardFour && cardFour != cardFive && cardFour != cardOne) ||
(cardThree == cardFour && cardFour == cardFive && cardFive != cardOne && cardFive != cardTwo) ||
(cardTwo == cardFour && cardFour == cardFive && cardFive != cardOne && cardFive != cardThree) ||
(cardOne == cardFour && cardFour == cardFive && cardFive != cardTwo && cardFive != cardThree)||
(cardOne == cardThree && cardThree == cardFive && cardFive != cardTwo && cardFive != cardFour)||
(cardOne == cardTwo && cardTwo == cardFive && cardFive != cardThree && cardFive != cardFour)||
(cardOne == cardTwo && cardTwo == cardFour && cardFour != cardThree && cardFour != cardFive) ||
(cardTwo == cardThree && cardThree == cardFive && cardFive != cardOne && cardFive != cardFour)||
(cardOne == cardThree && cardThree == cardFour && cardFour != cardTwo && cardFour != cardFive)){
threeKind++;
}
  
//now we have the if statement for a two pair 
else if ((cardOne == cardTwo && cardThree == cardFour && cardOne != cardFive && cardThree != cardFive) ||
(cardOne == cardThree && cardTwo == cardFour && cardOne != cardFive && cardTwo != cardFive) ||
(cardOne == cardFour && cardTwo == cardThree && cardOne != cardFive && cardTwo != cardFive) ||
(cardOne == cardThree && cardTwo == cardFour && cardOne != cardFive && cardTwo != cardFive) ||
(cardTwo == cardFour && cardThree == cardFive && cardOne != cardFive && cardTwo != cardOne) ||
(cardOne == cardFour && cardThree == cardFive && cardOne != cardTwo && cardTwo != cardFive) ||
(cardOne == cardTwo && cardFour == cardFive && cardOne != cardThree && cardFour != cardThree) ||
(cardOne == cardThree && cardTwo == cardFive && cardOne != cardFour && cardTwo != cardFour) ||
(cardOne == cardFive && cardThree == cardFour && cardOne != cardTwo && cardTwo != cardThree) ||
(cardOne == cardThree && cardFive == cardFour && cardOne != cardTwo && cardTwo != cardFive) ||
(cardOne == cardFour && cardTwo == cardFive && cardOne != cardThree && cardTwo != cardThree) ||
(cardOne == cardFive && cardTwo == cardThree && cardOne != cardFour && cardTwo != cardFour) ||
(cardOne == cardFive && cardTwo == cardFour && cardOne != cardThree && cardTwo != cardThree) ||
(cardTwo == cardThree && cardFour == cardFive && cardOne != cardFive && cardTwo != cardOne) ||
(cardTwo == cardFive && cardThree == cardFour && cardOne != cardFive && cardOne != cardThree)) {
twoPair++;
}
    
 //now we have the if statement for anything with a pair
else if ((cardOne == cardTwo && cardTwo != cardThree && cardTwo != cardFour && cardTwo != cardFive && cardThree != cardFour && cardThree != cardFive && cardFour != cardFive) ||
(cardThree == cardFour && cardFour != cardOne && cardFour != cardTwo && cardFour != cardFive && cardOne != cardTwo && cardOne != cardFive && cardTwo != cardFive) ||
(cardThree == cardFive && cardFour != cardOne && cardFour != cardTwo && cardFour != cardFive && cardOne != cardTwo && cardOne != cardFive && cardTwo != cardFive) ||
(cardOne == cardThree && cardTwo != cardThree && cardThree != cardFour && cardThree != cardFive && cardTwo != cardFour && cardTwo != cardFive && cardFour != cardFive) ||
(cardOne == cardFour && cardTwo != cardThree && cardTwo != cardFour && cardTwo != cardFive && cardThree != cardFour && cardThree != cardFive && cardFour != cardFive) ||
(cardTwo == cardFour && cardThree != cardOne && cardThree != cardFour && cardThree != cardFive && cardOne != cardFour && cardOne != cardFive && cardFour != cardFive) ||
(cardTwo == cardFive && cardThree != cardOne && cardThree != cardFour && cardThree != cardFive && cardOne != cardFour && cardOne != cardFive && cardFour != cardFive) ||
(cardFour == cardFive && cardFour != cardOne && cardFour != cardTwo && cardFour != cardThree && cardOne != cardTwo && cardOne != cardThree && cardTwo != cardThree) ||
(cardOne == cardFive && cardTwo != cardThree && cardTwo != cardFour && cardTwo!= cardFive && cardThree != cardFour && cardThree != cardFive && cardFour != cardFive) ||
(cardTwo == cardThree && cardThree != cardOne && cardThree != cardFour && cardThree != cardFive && cardOne != cardFour && cardOne != cardFive && cardFour != cardFive)){
onePair++;
}
//here we subtract one from the total amount of hands left to be dealt since a hand has now been dealt
totalHands--;
}
//now we must calculate the probabilites of each hand
    
double fourOfKind = (double)fourKind/hands;
double threeOfKind = (double)threeKind/hands;
double twoPair2 = (double)twoPair/hands;
double onePair2 = (double)onePair/hands;

//here we print the probabilities of each hand 
String.format("%.3g%n", fourOfKind);
String.format("%.3g%n", threeOfKind);
String.format("%.3g%n", twoPair2);
String.format("%.3g%n", onePair2);
System.out.println(hands + " hands were dealt.");
System.out.println("The probability of a four of a kind is " + fourOfKind);
System.out.println("The probability of a three of a kind is " + threeOfKind);
System.out.println("The probability of a two pair is " + twoPair2);
System.out.println("The probability of a pair is " + onePair2);

}
}