//Noah Haversat 
//October 23, 2018
//This program asks the user for how many rows they would like and then outputs an encrpyted x using
//stars to show the user

//first we must import the scanner
import java.util.Scanner;

//creating the class
public class EncryptedX {

//creating the main method
public static void main(String[] args) {
  
//declaring our scanner 
Scanner scan = new Scanner(System.in);

//asking user for input of how many rows they would like
System.out.println("What size grid would you like the X to be made of? ");
//declare and inititialize the input they give
int size = scan.nextInt();

//Creating the for loop that will run through and create each line of the encrypted X
for(int numberRows = 0; numberRows <= size; numberRows++){
for(int numberColumns = 0; numberColumns <= size; numberColumns++){

//this portion inputs the spaces that make up the x
if (numberRows == numberColumns || numberRows == size - numberColumns){
System.out.print(" ");}
else { System.out.print("*");}
}
System.out.println();
}
}
}