//Noah Haversat 
//Homework 7
//Ocotber 30 2018
//this program will prompt the user to input a text of their choosing and then provide options via a menu regarding options for the text
//the program will loop allowing the user to recieve multiple outputs from the menu from the same text

//importing the scanner
import java.util.Scanner;

//creating the main method for our program
public class WordTools {
   
  //these are introduced across all methods
  static String text;
    static Scanner sc;
    public static void main(String[] args) {
        text = "";
        sc = new Scanner(System.in);
        
        sampleText();
 //here the menu for our text is formed that will be used for after the user inputs their text of choice 
        boolean loop = true;
        while (loop) {
         String choice = printMenu();
         if (choice.equals("q")) {
          loop = false;
         }
         else if (choice.equals("c")) {
          System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(text));
         }
         else if (choice.equals("w")) {
          System.out.println("Number of words: " + getNumOfWords(text));
         }
         else if (choice.equals("f")) {
          System.out.println("Enter a word or phrase to be found: ");
          String find = sc.nextLine();
          System.out.println(find + " instances: " + findText(find, text));
         }
         else if (choice.equals("r")) {
          System.out.println("Edited text: " + replaceExclamation(text));
         }
         else if (choice.equals("s")) {
          System.out.println("Edited text: " + shortenSpace(text));
         }
         System.out.println();
        }
        System.out.println("Goodbye.");
    }
// this method calls the user to input his or her text
    public static void sampleText() {
        System.out.println("Enter a sample text: ");
        if (sc.hasNext()) {
            text = sc.nextLine();
        }
        System.out.println("You entered: " + text);
    }
//this method prints the menu for the user everytime they enter a text
    public static String printMenu() {
        System.out.println("MENU");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit");
        System.out.println("Choose and option: ");
        String choice = sc.nextLine();
        return choice;
    }
 //this method determines the number of non white spaces for the text  
    public static int getNumOfNonWSCharacters(String input) {
     int count = 0;
     for (int i = 0; i < input.length(); i++) {
      if (input.charAt(i) != ' ') {
       count = count + 1;
      }
     }
     return count;
    }
//this method determines the number of words that are in the text 
    public static int getNumOfWords(String input) {
     int count = 0;
     for (int i = 0; i < input.length(); i++) {
      if (input.charAt(i) == ' ') {
       count = count + 1;
      }
     }
     count = count + 1;
     return count;
    }

    //this method allows the user to find certain things in the text they inputted
    public static int findText(String find, String text) {
     int count = 0;
     for (int i = 0; i < text.length()-find.length()+1; i++) {
      String piece = text.substring(i, i+find.length());
      if (find.equals(piece)) {
       count = count + 1;
      }
     }
     return count;
    }
 //this method allows the user to replace any !'s from the text 
    public static String replaceExclamation(String input) {
     String ret = "";
     for (int i = 0; i < input.length(); i++) {
      if (input.charAt(i) == '!') {
       ret += '.';
      }
      else {
       ret += input.charAt(i);
      }
     }
     return ret;
    }
 //this method shortens the space for the text  
    public static String shortenSpace(String input) {
     String ret = "";
     for (int i = 0; i < input.length(); i++) {
      if (input.charAt(i) == ' ') {
       ret += input.charAt(i);
       while (input.charAt(i+1) == ' ') {
        i++;
       }
      }
      else {
       ret += input.charAt(i);
      }
     }
     return ret;
    }
}