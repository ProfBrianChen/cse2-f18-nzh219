//Noah Haversat 
//November 15 2018 
//This program shuffles a deck of cards prints out all the cards as they are shhuffled
//and then prints out a hand from the deck

//first we import the scanner the randomizer and enter the class
import java.util.Scanner;
import java.util.Random;
public class HomeWork8 {

//creating the array for the shuffled deck of cards
static String[] cards;

//this method shuffles the cards and places them in the array 
 public static String[] shuffle(String[] card){
        Random rand = new Random();
        for(int k = 0; k < 51; k++){
            int Q = Math.abs(rand.nextInt()) % 52;
            String deck = card[Q];
            card[Q] = card[k];
            card[k] = deck;
        }
        return card;
    }

 //this method retreives a hand from the end of the deck  
 public static String[] getHand(String[] list, int index, int numCards){
        String[] hand = new String[numCards];
        for(int k = index; k > index - numCards; k--){
            hand[index-k] = list[k];
        }
        return hand;
    }  
 //this prints the array of cards out  
  public static void printArray(String [] card){
        for(int k = 0; k < card.length; k++){
            System.out.print(card[k] + " ");
        }
        System.out.println("");
    }

  
  
  //this is the main method that was given to us
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // suits club, heart, spade or diamond
        String[] suitNames = { "C", "H", "S", "D" };
        String[] rankNames = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };
        cards = new String[52];
        String[] hand = new String[5];
        int numCards = 5;
        int again = 1;
        int index = 51;
        for (int i = 0; i < 52; i++) {
            cards[i] = rankNames[i % 13] + suitNames[i / 13];
            System.out.print(cards[i] + " ");
        }
        System.out.println();
        printArray(cards);
        cards = shuffle(cards);
        printArray(cards);
        System.out.println("Hand");
        while (again == 1) {
            hand = getHand(cards, index, numCards);
            printArray(hand);
            index = index - numCards;
            System.out.println("Enter a 1 if you want another hand drawn");
            again = scan.nextInt();
        }
    }

   

    
}