import java.util.Scanner;

public class RemoveElements {
    
//This code was given to us by Professor Carr
public static void main(String[] arg) {
        Scanner scan = new Scanner(System.in);
        int num[] = new int[10];
        int newArray1[];
        int newArray2[];
        int index, target;
        String answer = "";
        do {
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput();
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            while(index < 0 || index > num.length-1){
                System.out.println("Enter a valid index");
                index = scan.nextInt();
            }
            newArray1 = delete(num, index);
            String out1 = "The output array is ";
            out1 += listArray(newArray1); // return a string of the form "{2, 3, -9}"
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            while(target < 0 || target > num.length-1){
                System.out.println("Enter a valid index");
                target = scan.nextInt();
            }
            newArray2 = remove(num, target);
            String out2 = "The output array is ";
            out2 += listArray(newArray2); // return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer = scan.next();
        } while (answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]) {
        String out = "{";
        for (int j = 0; j < num.length; j++) {
            if (j > 0) {
                out += ", ";
            }
            out += num[j];
        }
        out += "} ";
        return out;
    }

   
 //this method creates an array that is made up oof random elements in the range of 0-9 and returns the array to the main method
public static int[] randomInput() {
int[] arrayOne = new int[10];
for (int k = 0; k < arrayOne.length; k++) {
arrayOne[k] = (int) (Math.random() * arrayOne.length);
}
return arrayOne;
}
 

//this method deletes the elements that are equal to the users target number and then recreates the new array 
 public static int[] remove(int[] list, int target) {
 int counter = 0;
 for (int k = 0; k < list.length; k++) {
 if (list[k] == target) {
 counter++;
 list[k] = -1;
 }
 }
 int[] arrayTwo = new int[list.length - counter];
 int tempVal = 0;
 for(int k = 0; k < list.length; k++){
 if(list[k] != -1){
 arrayTwo[tempVal] = list[k];
 tempVal++;
}
}
return arrayTwo;
}


//this method is used to delete the element of the array that the use chooses and then recreates the array 
 public static int[] delete(int[] list, int pos) {
 int[] arrayThree = new int[list.length - 1];
 for (int k = 0; k < list.length; k++) {
 if (k < pos) {
 arrayThree[k] = list[k];
 } else if (k > pos) {
 arrayThree[k - 1] = list[k];
 }
 }
 return arrayThree;
 }



}
