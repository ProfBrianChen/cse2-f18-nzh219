// Noah Haversat 
// Lab 02
// CSE 02
// September 12, 2018


//this program is meant to calculate certain characteristcs based off 
// of user inputed data

public class Cyclometer { 
  // maine method require for every java program 
  public static void main (String [] args){
    
    // in this section we will declare and input all of our data 
    int secsTrip1 = 480; //how long trip one took 
    int secsTrip2 = 3220; //how long trip two took 
    int countsTrip1 = 1561; //how many rotations the wheel made for trip one
    int countsTrip2 = 9037; //how many rotations the wheel made for trip two 
    
    // this section will declare all of our constants for the problem
    
    double wheelDiameter = 27.0; // the diameter of the bike wheel 
    double PI = 3.14159; // defines pi
    int feetPerMile = 5280; //defines the length of a mile in feet
    int inchesPerFoot = 12; // defines how many inches in a foot
    int secondsPerMinute = 60; //defines how many seconds are in a minute
    
    // this section declares the undefined variabes 
    
    double distanceTrip1; //undefined variable for length of trip
    double distanceTrip2; //undefined variable for length of trip
   
    // this section calculates and prints the total time that each trip took
    
    System.out.println("Trip one took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");  //function printing out characteristics of each trip
    System.out.println("Trip two took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");  //function printing out characteristics of each trip
      
  // this section calcualtes the distance that each trip covered
      
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // defining the distance for trip one in inches
    distanceTrip2 = countsTrip2 * wheelDiameter * PI; // defining the distance for trip two in inches 
    
    // changing the units from inches to miles
    
    int inchesPerMile = feetPerMile * inchesPerFoot; // defines how many inches are in a foot 
    
    distanceTrip1 = distanceTrip1 / inchesPerMile;  //calculates the distance in miles
    distanceTrip2 = distanceTrip2 / inchesPerMile; //calculates the distance in miles
    
    double totalDistance = distanceTrip2 + distanceTrip1; //defines total distance of trip 
    
    // this section prints out the data we have derived
    
  System.out.println("Trip 1 was "+distanceTrip1+" miles"); //prints how long trip one was
	System.out.println("Trip 2 was "+distanceTrip2+" miles"); //prints how long trip two was
	System.out.println("The total distance was "+totalDistance+" miles"); //prints combined distances of the trips 

    
    
    
  }
}