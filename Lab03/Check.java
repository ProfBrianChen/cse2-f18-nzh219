// this lab aims to split a bill between a certain amount of poeple

// the program will ask for the total bill, tip percentage, and how many people to split the bill between

// must import the scanner class in order to avoid receiving errors

import java.util.Scanner; // importing the scanner 

// here the program opens in standard java formatting 

public class Check { 
      public static void main (String[] args ) { 
        
          Scanner myScanner = new Scanner(System.in); // declaring scanner and calling constructor 
        
          System.out.print("Enter the original cost of the check in the form of xx.xx ");  //prompting the user to input the cost of the check 
        
          double checkCost = myScanner.nextDouble(); //creating a variable check cost that is equal to the input given by the user 
          
          System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx"); //prompting the user to input the percentage they wish to tip 
       
          double tipPercent = myScanner.nextDouble(); // creating a new variable for the tip percentage 
          tipPercent /= 100; //puts tip percent in decimal formatting
            
          System.out.print("Enter the number of people who went out to dinner: "); //asks user to input how many people went to dinner 
        
          int numPeople = myScanner.nextInt(); //assigns amount of people to what user inputs 
        
          double totalCost; //creates variable for the total cost of the meal 
          double costPerPerson; //creates variable for the total cost of each person
          int dollars, dimes, pennies; //this is for storing digits to the right of the decimal sign 
        
          totalCost = checkCost * (1 + tipPercent); //calculates the total cost of the meal including the tip 
          costPerPerson = totalCost / numPeople; //calculates how much each person needs to pay 
          dollars = (int)costPerPerson; //so everyone pays a whole dollar amount
        
        // gets dimes amount, dropping decimal fraction
        
        dimes = (int)(costPerPerson * 10) % 10; //ensures a whole number of dimes
        pennies = (int)(costPerPerson * 100) % 10; //ensures a whole number of pennies
        System.out.println("Each person in the group owes $ " + dollars + '.' + dimes + pennies); //prints what amount everyone owes
        
        //printing how much each person owes for the meal
        
        
        
        
      }  // end of main method 
}  // end of class