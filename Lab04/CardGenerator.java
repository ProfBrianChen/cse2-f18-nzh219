// Noah Haversat, CSE2 Lab 4, September 21, 2018

//This lab aims to produce the number and suit of a card picked randomly from a deck.This

//First we must import the math functions into the program 
import java.util.Random;

public class CardGenerator{
   public static void main(String[] args){ //creates string for the program

   
    Random rand = new Random(); 
    int card = rand.nextInt(52) + 1;//creates value of card
     
     
     //this if statement will seperate the cards into their respective suits, the first being diamonds 
    if (card < 14) { //only numbers up to 13 enter the if statement
      card = card;
    // accounting for the face cards and ace
        if (card == 11){System.out.println("Jack of diamonds");}
        else if (card == 12){System.out.println("Queen of diamonds");}
        else if (card == 13){System.out.println("King of diamonds");}
        else if (card == 1){System.out.println("Ace of diamonds");}
        else {System.out.println(card + " of diamonds");} //prints any non face cards
     }
   
   //this if statement will seperate the cards into the clubs suit
     
   
   if (card > 13 && card < 27){
     card = card - 13;//changing format to range of 1 to 13
        // this prints for the face cards  
     if (card == 11){System.out.println("Jack of clubs");}
        else if (card == 12){System.out.println("Queen of clubs");}
        else if (card == 13){System.out.println("King of clubs");}
        else if (card == 1){System.out.println("Ace of clubs");}
        else {System.out.println(card + " of clubs");} //prints any non face cards
     }
     
     //this if statement will seperate the cards into the heart suit
     
 if (card > 26 && card < 40){
   card = card - 26; //changing format to range of 1 to 13
        // this prints for the face cards  
   if (card == 11){System.out.println("Jack of hearts ");}
        else if (card == 12){System.out.println("Queen of hearts");}
        else if (card == 13){System.out.println("King of hearts");}
        else if (card == 1){System.out.println("Ace of hearts");}
        else {System.out.println(card + " of hearts");} //prints any non face cards
     }
    
     //this is statement will seperate the cards into the spades suit
     
   if (card > 39 && card < 53){
   card = card - 39; //changing format to range of 1 to 13
     // this prints for the face cards   
     if (card == 11){System.out.println("Jack of spades ");}
        else if (card == 12){System.out.println("Queen of spades");}
        else if (card == 13){System.out.println("King of spades");}
        else if (card == 1){System.out.println("Ace of spades");}
        else {System.out.println(card + " of spades");} //prints any non face cards
     }
    
   
  }
}
    

       