//Noah Haversat
//Lab05
//October 11 2018 
//this lab asks the user for the course number, department name, the number of times it meets in a week
//the number of times it meets in a week, the time the class starts, the instructor name, and the number
//of students

//for each item check to make sure teh user enters the correct type of information.  If not use the loop to 
//ask again.  If an integar is asked for and a double is given print an error message saying they need to 
//type in a caertain type of variable 


//first we need to import a scanner and random function 
import java.util.Scanner;

//next we create our main method and class
public class Lab05 {
public static void main(String[] args){

//creating the scanner
Scanner scan = new Scanner(System.in);

//ask the user for the course number
System.out.println("What is the course number for the class?");

//check if the user enters an integar 
Boolean course = scan.hasNextInt();
while (course == false) { 
Scanner scan2 = new Scanner(System.in);
System.out.println("Invalid please enter a valid course number");
course = scan2.hasNextInt();}


//creating the scanner
Scanner scan3 = new Scanner(System.in);
//Ask the user for the name of the department 
System.out.println("What is the name of the department?");

//Creating a boolean for the department 
Boolean departmentName = scan3.hasNext("[a-zA-Z]+");
while (departmentName == false){
Scanner scan2 = new Scanner(System.in);
System.out.println("Invalid name please enter a new department");
departmentName = scan2.hasNext("[a-zA-Z]+");}

//Ask the user for the number of times the class meets per week 
System.out.println("How many times does the class meet per week?");
//creating the scanner
Scanner scan4 = new Scanner(System.in);

//Creating the boolean for meeting times per week
Boolean meetingTime = scan4.hasNextInt();
while (meetingTime == false){
Scanner scan2 = new Scanner(System.in);
System.out.println("Invlaid input please enter an integar");
meetingTime = scan2.hasNextInt();}
                  

//creating the scanner
Scanner scan5 = new Scanner(System.in);
//Ask the user for what time the class starts 
System.out.println("What time does the class start?");

//Creating Boolean for what time the class starts
Boolean classStart = scan5.hasNextInt();
while(classStart == false){
Scanner scan2 = new Scanner(System.in);
System.out.println("Invalid class time please enter a valid time in the form XXXX");
classStart = scan2.hasNextInt();}

//creating the scanner
Scanner scan6 = new Scanner(System.in);
//Ask the user for the instructor name 
System.out.println("What is the name of the instructor?");

//Creating Boolean for the instructors name 
Boolean teacherName = scan6.hasNext("[a-zA-Z]+");
while (teacherName == false){
Scanner scan2 = new Scanner(System.in);
System.out.println("Invalid name please enter the name of the instructor");
teacherName = scan2.hasNext("[a-zA-Z]+");}

//creating the scanner
Scanner scan7 = new Scanner(System.in);
//Ask the user for how many students are in the class 
System.out.println("How many students are in the class?");

//Creating the boolean for the amount of students in the class
Boolean classStudents = scan7.hasNextInt();
while (classStudents == false){
Scanner scan2 = new Scanner(System.in);
System.out.println("Invalid input please enter an integer");
classStudents = scan2.hasNextInt();}


}
}

