//Noah Haversat 
//November 1 2018
//This program allows the user to create random sentences using multiple methods to genrate differnt types of words

//importing the random number generator 
import java.util.Random;

//our main method 
class LabSeven {
    static Random randomGenerator;
    public static void main(String[] args){
        randomGenerator = new Random();
        finalSentence();
}

  //this method creates the string of words that the random number generator will create
  public static void finalSentence(){
        String subject = subject();
        String object = object();
        int words  = randomGenerator.nextInt(10);
        System.out.println("The " + subject + " " + verb() + " the " + adj() + " " + object + ".");
        for(int i = 0; i < words; i++){
            actionSentence(subject);
        }
        System.out.println("The " + subject + " " + finalVerb() + " the " + object + "!");
    }

   
//his method determines the subject for the sentences
    public static String subject(){
        int words = randomGenerator.nextInt(10);
        String string = "";
        switch(words) {
            case 1: string = "dog";
            break;
            case 2: string = "cat";
            break;
            case 3: string = "fox";
            break;
            case 4: string = "fish";
            break;
            case 5: string = "professor";
            break;
            case 6: string = "mother";
            break;
            case 7: string = "father";
            break;
            case 8: string = "horse";
            break;
            default: string = "human";
            break;
        }
        return string;
    }

    public static String verb(){
        int words = randomGenerator.nextInt(10);
        String string = "";
        switch(words) {
            case 1: string = "ran";
            break;
            case 2: string = "bit";
            break;
            case 3: string = "cut";
            break;
            case 4: string = "smashed";
            break;
            case 5: string = "stole";
            break;
            case 6: string = "ate";
            break;
            case 7: string = "drove";
            break;
            case 8: string = "chased";
            break;
            default: string = "took";
            break;
        }
        return string;
    }

   public static String finalVerb(){
        int words = randomGenerator.nextInt(10);
        String string = "";
        switch(words) {
            case 1: string = "hated";
            break;
            case 2: string = "carried";
            break;
            case 3: string = "beheaded";
            break;
            case 4: string = "pushed";
            break;
            case 5: string = "showed";
            break;
            case 6: string = "liked";
            break;
            case 7: string = "hid";
            break;
            case 8: string = "ran";
            break;
            default: string = "showered";
            break;
        }
        return string;
    }
  
  
  public static String object(){
        int words = randomGenerator.nextInt(10);
        String string = "";
        switch(words) {
            case 1: string = "rock";
            break;
            case 2: string = "tree";
            break;
            case 3: string = "door";
            break;
            case 4: string = "house";
            break;
            case 5: string = "bag";
            break;
            case 6: string = "shirt";
            break;
            case 7: string = "pants";
            break;
            case 8: string = "shoes";
            break;
            default: string = "socks";
            break;
        }
        return string;
    }

   
  
   public static String adj(){
        int words = randomGenerator.nextInt(10);
        String string = "";
        switch(words) {
            case 1: string = "long";
            break;
            case 2: string = "fat";
            break;
            case 3: string = "hairy";
            break;
            case 4: string = "heavy";
            break;
            case 5: string = "warm";
            break;
            case 6: string = "cold";
            break;
            case 7: string = "tall";
            break;
            case 8: string = "short";
            break;
            default: string = "smelly";
            break;
           
        }
        return string;
    }

    public static void actionSentence(String subject){
        int words = randomGenerator.nextInt(1);
        if(words == 0){
            System.out.println("It " + verb() + " the " + adj() + " " + object() + ".");
        } else {
            System.out.println("The " + subject +  " " +verb() +" the " + adj() + " " + object()+ ".");
        }
    }
}