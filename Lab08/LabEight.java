//Noah Haversat 
//November 12 2018 
//Lab 8: arrays
//This program aims to randomly place positive integars greater than zero and less than 100 into an array 
//and then count how many times each number is repeated in the array 

//creating our class
public class LabEight{
  
//this method counts the integars that are repeated within the random array 
public static int[] integarCount(int[] ints){
//creating an array to hold the occurances
int[] occurance = new int[100];
//for loop that runs through the array and adds to the occurances of each integar 
for(int j = 1; j <= occurance.length; j++)
for(int k=0; k < ints.length; k++)
if(ints[k] == j)
occurance[j-1]++;
return occurance;}  
 
//entering the main method 
public static void main(String[] args){
//this creates the first array with 100 elements from 0 -99
int[] randomArray = new int[100];
for(int j = 0; j < randomArray.length; j++)
randomArray[j] =(int)(Math.random() * 99 +1);
int[] occurance = integarCount(randomArray);
displayIntCount(occurance);}



//this array prints out the information stored in the array created by the method above 
public static void displayIntCount(int[] occurance){
for (int j = 0; j < occurance.length; j++)
System.out.println((j+1) +" occurs " +occurance[j] +" times");}
}
